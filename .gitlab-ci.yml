include:
  - template: Dependency-Scanning.gitlab-ci.yml
  - template: License-Scanning.gitlab-ci.yml

# TO DO ! https://gitlab.com/help/user/application_security/dast/index#configuration
# include:
#   template: DAST.gitlab-ci.yml

# TO DO ! https://gitlab.com/help/user/application_security/container_scanning/index
# include:
#   template: Container-Scanning.gitlab-ci.yml

stages:
  - test
  - base
  - build
  - release
  - deploy

variables:
  POSTGRES_DB: test_db
  POSTGRES_USER: runner
  POSTGRES_PASSWORD: ""
  POSTGRES_HOST_AUTH_METHOD: trust
  BASE_IMAGE: $CI_REGISTRY_IMAGE/base:$CI_COMMIT_REF_SLUG
  BACKEND_IMAGE: $CI_REGISTRY_IMAGE/backend:$CI_COMMIT_REF_SLUG
  SIDEKIQ_IMAGE: $CI_REGISTRY_IMAGE/sidekiq:$CI_COMMIT_REF_SLUG
  BASE_RELEASE_IMAGE: $CI_REGISTRY_IMAGE/base:latest
  BACKEND_RELEASE_IMAGE: $CI_REGISTRY_IMAGE/backend:latest
  SIDEKIQ_RELEASE_IMAGE: $CI_REGISTRY_IMAGE/sidekiq:latest
  # DAST_WEBSITE: https://jogl-backend-dev.herokuapp.com

cache:
  key: ${CI_COMMIT_REF_SLUG}
  paths:
  - vendor/ruby

.shared_hidden_key: &testing  # This is an anchor
  image: "ruby:2.7.2"
  stage: test
  before_script:
    - apt-get install -y curl
    - ruby -v                                   # Print out ruby version for debugging
    - gem install bundler
    # Repo for more recent version of node
    - curl -sL https://deb.nodesource.com/setup_14.x | bash -
    - apt-get install -y nodejs gcc g++ make
    - npm i
    # - RAILS_ENV=test bundle config set path 'vendor'
    - RAILS_ENV=test bundle install -j $(nproc) --path ./vendor  # Install dependencies into ./vendor/ruby
  services:
    - postgres:latest
    - redis:latest
  # Cache gems in between builds
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - vendor

license_scanning:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push" || $CI_COMMIT_BRANCH != "develop"'
      when: never

rspec:
  <<: *testing
  script:
  - cp config/database.yml.gitlab config/database.yml
  - RAILS_ENV=test bundle exec rails db:migrate
  - RAILS_ENV=test bundle exec rails db:seed
  - RAILS_ENV=test bundle exec rails db:seed:interests
  - RAILS_ENV=test bundle exec rails spec
  artifacts:
    paths:
      - coverage/

.shared_hidden_key: &building  # This is an anchor
  image: "docker:stable"
  services:
    - docker:20.10.3-dind #dind => docker in docker
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

base:
  <<: *building
  stage: base
  only:
    refs:
      - develop
      - master
    changes:
      - Gemfile
      - Gemfile.lock
      - package.json
      - package-lock.json
  script:
    - docker pull $BASE_IMAGE || true
    - docker build -f docker/Dockerfile . --build-arg BUILDKIT_INLINE_CACHE=1 --build-arg RAILS_ENV=$RAILS_ENV --cache-from=$BASE_IMAGE --target base -t $BASE_IMAGE
    - docker push $BASE_IMAGE

base-release:
  <<: *building
  stage: release
  only:
    refs:
      - master
  script:
    - docker pull $BASE_IMAGE
    - docker tag $BASE_IMAGE $BASE_RELEASE_IMAGE
    - docker push $BASE_RELEASE_IMAGE

build:
  <<: *building
  stage: build
  only:
    refs:
      - develop
      - master
  script:
    - docker pull $BASE_IMAGE
    - docker pull $BACKEND_IMAGE
    - docker pull $SIDEKIQ_IMAGE
    - docker build -f docker/Dockerfile . --build-arg RAILS_ENV=$RAILS_ENV --cache-from=$BASE_IMAGE --cache-from=$BACKEND_IMAGE --target web -t $BACKEND_IMAGE
    - docker build -f docker/Dockerfile . --build-arg RAILS_ENV=$RAILS_ENV --cache-from=$BASE_IMAGE --cache-from=$SIDEKIQ_IMAGE --target sidekiq -t $SIDEKIQ_IMAGE
    - docker push $BACKEND_IMAGE
    - docker push $SIDEKIQ_IMAGE

release:
  image: "docker:stable"
  stage: release
  services:
    - docker:20.10.3-dind
  only:
    refs:
      - master
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker pull $BACKEND_IMAGE
    - docker pull $SIDEKIQ_IMAGE
    - docker tag $BACKEND_IMAGE $BACKEND_RELEASE_IMAGE
    - docker tag $SIDEKIQ_IMAGE $SIDEKIQ_RELEASE_IMAGE
    - docker push $BACKEND_RELEASE_IMAGE
    - docker push $SIDEKIQ_RELEASE_IMAGE

pages:
  stage: deploy
  dependencies:
    - rspec
  script:
    - mv coverage/ public/
  artifacts:
    paths:
      - public
  only:
    - master

heroku-deploy-develop:
  only:
    refs:
    - develop
  image: "docker:stable"
  services:
  - docker:20.10.3-dind
  stage: deploy
  environment:
    name: develop
  script:
    - sh release.sh

heroku-deploy-master:
  only:
    refs:
    - master
  image: "docker:stable"
  services:
  - docker:20.10.3-dind
  stage: deploy
  environment:
    name: master
  script:
    - sh release.sh
