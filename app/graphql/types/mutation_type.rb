# frozen_string_literal: true

module Types
  class MutationType < Types::BaseObject
    field :mark_all_notifications_as_read, mutation: Mutations::MarkAllNotificationsAsRead
    field :mark_notification_as_read, mutation: Mutations::MarkNotificationAsRead
  end
end
