# frozen_string_literal: true

require 'digest/sha2'
class ApplicationMailer < ActionMailer::Base
  default 'message-id' => "<#{Digest::SHA2.hexdigest(Time.now.to_i.to_s)}@jogl.io>",
          from: "JOGL - Just One Giant Lab <#{ENV['JOGL_NOTIFICATIONS_EMAIL']}>"
  # layout 'mailer'
  layout 'mjml_notifications_mailer'
end
