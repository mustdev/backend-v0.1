# frozen_string_literal: true

class InviteStrangerMailer < ApplicationMailer
  def invite_stranger(owner, object, stranger_email)
    @to = User.new(email: stranger_email)
    @owner = owner
    @object = object
    @from_object = object.class.name.downcase != 'community' ? object.class.name.downcase : 'group'
    @stranger = stranger_email
    # Postmark metadatas
    metadata['object-type'] = @from_object
    mail(to: "<#{stranger_email}>", subject: 'You have been invited to join a ' + @from_object, tag: 'invite-stranger')
  end
end
