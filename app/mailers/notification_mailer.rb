# frozen_string_literal: true

# TODO: test that mail is sent
# todo: internationalization

# TODO: bell notification on frontend
#
class NotificationMailer < ApplicationMailer
  # * for if/when we shift to Postmark Templates
  # include PostmarkRails::TemplatedMailerMixin
  def notify(notification)
    # ! @to is used in the mailer layout
    @to = notification.target
    @target = notification.target
    @object = notification.object
    @author = User.find(notification.metadata[:author_id]) if notification.metadata.key?(:author_id)
    @followed_thing = @object.respond_to?(:feed) && @object.feed.feedable
    @notification = notification

    # Postmark metadatas
    metadata['user-id-to'] = @target.id

    mail(
      to: "<#{@target.email}>",
      subject: notification.subject_line,
      template_path: 'notifications',
      template_name: @notification.type,
      tag: @notification.type,
      message_stream: 'notifications' # Postmark
    )
  end
end
