# frozen_string_literal: true

class RecsysMailer < ApplicationMjmlMailer
  @sanitizer = Rails::Html::WhiteListSanitizer.new

  def sanitize(string)
    @sanitizer.sanitize(string, tags: [])
  end

  def location(need)
    return "#{need.city}, #{need.country}" unless need.city.nil? || need.country.nil?
    return need.city.to_s unless need.city.nil?
    return need.country.to_s unless need.country.nil?

    ''
  end

  def recsys_recommendation(user, campaign_id, needs, scores)
    @to = user # used in layout
    @needs = needs
    @campaign_id = campaign_id
    @scores = scores
    # Postmark metadatas
    metadata['user-id-to'] = @to.id
    metadata['needs'] = @needs.map(&:id).join(', ')
    metadata['campaign_id'] = @campaign_id
    metadata['scores'] = scores.join(', ')
    mail(
      to: "#{@to.first_name} #{@to.last_name} <#{@to.email}>",
      from: "JOGL <#{ENV['JOGL_NOTIFICATIONS_EMAIL']}>",
      subject: "We've been thinking about you! 👋",
      tag: 'needs-recommendations',
      &:html
    )
  end
end
