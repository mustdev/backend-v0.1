# frozen_string_literal: true

class UserMailer < ApplicationMailer
  def registration_confirmation(user, redirect_url)
    @to = user
    @user = user
    @redirect_url = redirect_url
    # Postmark metadatas
    metadata['user-id-to'] = @user.id
    mail(to: "<#{user.email}>", subject: 'Registration Confirmation', tag: 'confirm-account')
  end
end
