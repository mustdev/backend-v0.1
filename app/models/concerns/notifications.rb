# frozen_string_literal: true

module Notifications
  extend ActiveSupport::Concern

  module ClassMethods
    # we're overriding `for_group` here because we need to reference
    # our Notification class that inherits from NotificationHandler::Notification,
    # otherwise, our subclass's methods don't get called.
    def for_group(group, args: [], attrs: {})
      return if group.nil?

      target_scope = NotificationHandler::Group.find_by_name!(group).target_scope
      target_scope.call(*args)&.map do |target|
        Notification.create(attrs.merge(target: target))
      end
    end
  end
end
