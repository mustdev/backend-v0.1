# frozen_string_literal: true

class Customdata < ApplicationRecord
  belongs_to :customfield
  belongs_to :user
end
