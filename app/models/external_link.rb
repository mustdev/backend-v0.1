class ExternalLink < ApplicationRecord
  has_one_attached :icon
  belongs_to :linkable, polymorphic: true, optional: true

  validates :url, :name, presence: true

  def icon_url(resize = '64x64')
    return icon_variant_url(resize: resize) if icon.attached? && icon.variable?
    default_icon_url
  end

  def default_icon_url
    # NOTE: the below will add an asset digest, so
    # URLS may break if assets are changed in the future
    ActionController::Base.helpers.image_url("links-#{name}.png")
  end

  private

  def icon_variant_url(resize:)
    Rails
      .application
      .routes
      .url_helpers
      .rails_representation_url(icon.variant(resize: resize))
  end

end
