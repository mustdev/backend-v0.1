# frozen_string_literal: true

class Relation < ApplicationRecord
  belongs_to :user
  belongs_to :resource, polymorphic: true

  after_commit :reindex

  scope :clapped, -> { where(has_clapped: true) }
  scope :follows, -> { where(follows: true) }
  scope :saved, -> { where(saved: true) }
  scope :reviewed, -> { where(reviewed: true ) }
  scope :user_follows, -> { follows.where(resource_type: 'User') }
  scope :of_type, ->(type) { where(resource_type: type) }

  # This takes care of reindexing an objects
  # follower clap and saved count after someone did something on those
  def reindex
    resource&.index!
  end
end
