# frozen_string_literal: true

class Api::ClappersSerializer < ActiveModel::Serializer
  include Api::Relationsserializerhelper
  attributes :clappers

  def clappers
    object
      .relations
      .clapped
      .includes(:user)
      .map do |relation|
        user = relation.user
        # TODO: Remove FactoryBot from specs ? Kori mentionned this, but I am unsure what exactly this meant
        user = Users::DeletedUser.new unless user.active?
        data = {
          id: user.id,
          first_name: user.first_name,
          last_name: user.last_name,
          nickname: user.nickname,
          logo_url: user.logo_url,
          logo_url_sm: user.logo_url_sm,
          has_followed: has_followed(user),
          has_clapped: has_clapped(user),
          short_bio: user.short_bio
        }
        if object.instance_of?(Post)
          data = data.merge({
                              can_contact: user.can_contact,
                              projects_count: user.projects_count
                            })
        end
        data
      end
  end
end
