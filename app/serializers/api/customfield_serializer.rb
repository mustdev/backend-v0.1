# frozen_string_literal: true

class Api::CustomfieldSerializer < ActiveModel::Serializer
  # include Api::Usersserializerhelper
  # include Api::Utilsserializerhelper
  # include Api::Relationsserializerhelper

  attributes  :id,
              :name,
              :description,
              :field_type
end
