# frozen_string_literal: true

# TODO: refactor to just call 'resource' with a compact
# take a look at saved_serializer for an example
class Api::FollowingSerializer < ActiveModel::Serializer
  has_many :projects do
    object.following.where(resource_type: 'Project').select { |project| Project.exists? project.resource_id }.map do |project|
      @obj = Project.find(project.resource_id)
    end
  end

  has_many :communities do
    object.following.where(resource_type: 'Community').select { |community| Community.exists? community.resource_id }.map do |community|
      @obj = Community.find(community.resource_id)
    end
  end

  has_many :users do
    object.following.where(resource_type: 'User').select { |user| User.exists? user.resource_id }.map do |user|
      @obj = User.find(user.resource_id)
    end
  end

  has_many :challenges do
    object.following.where(resource_type: 'Challenge').select { |challenge| Challenge.exists? challenge.resource_id }.map do |challenge|
      @obj = Challenge.find(challenge.resource_id)
    end
  end

  has_many :programs do
    object.following.where(resource_type: 'Program').select { |program| Program.exists? program.resource_id }.map do |program|
      @obj = Program.find(program.resource_id)
    end
  end

  has_many :needs do
    object.following.where(resource_type: 'Need').select { |need| Need.exists? need.resource_id }.map do |need|
      @obj = Need.find(need.resource_id)
    end
  end
end
