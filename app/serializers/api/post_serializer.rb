# frozen_string_literal: true

class Api::PostSerializer < ActiveModel::Serializer
  include Api::Usersserializerhelper
  include Api::Utilsserializerhelper
  include Api::Relationsserializerhelper

  attributes :id,
             :content,
             :media,
             :creator,
             :mentions,
             :from,
             :comments,
             :created_at,
             :documents,
             :claps_count,
             :saves_count,
             :clappers

  attribute :has_clapped, unless: :scope?
  attribute :has_saved, unless: :scope?
  has_many :comments
  has_many :mentions

  def from
    if object.from_object.nil?
      {
        object_type: 'impossible',
        object_id: 0,
        object_name: 'Bollocks'
      }
    else
      {
        object_type: object.from_object.downcase,
        object_id: object.from_id,
        object_name: object.from_name,
        object_image: object.from_image,
        object_need_proj_id: object.from_need_project_id
      }
    end
  end
end
