# frozen_string_literal: true

module Api::Rolesserializerhelper
  # Verify user role for a given object
  def is_owner(obj = nil)
    obj = object if obj.nil?
    if current_user.nil?
      false
    else
      current_user.has_role?(:owner, obj)
    end
  end

  def is_admin(obj = nil)
    obj = object if obj.nil?
    if current_user.nil?
      false
    else
      current_user.has_role?(:admin, obj) || current_user.has_role?(:admin)
    end
  end

  def is_member(obj = nil)
    obj = object if obj.nil?
    if current_user.nil?
      false
    else
      current_user.has_role?(:member, obj)
    end
  end

  def is_reviewer(obj = nil)
    obj = object if obj.nil?
    if current_user.nil?
      false
    else
      current_user.has_role?(:reviewer, obj) || current_user.has_role?(:reviewer)
    end
  end

  def is_pending(obj = nil)
    obj = object if obj.nil?
    if current_user.nil?
      false
    else
      current_user.has_role?(:pending, obj)
    end
  end
end
