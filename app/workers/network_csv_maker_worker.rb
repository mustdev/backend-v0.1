# frozen_string_literal: true

require 'concerns/network_csv_maker'

class NetworkCsvMakerWorker
  include Sidekiq::Worker

  def perform
    maker = NetworkCsvMaker.new
    maker.create_network_snapshot
  end
end
