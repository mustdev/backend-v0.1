# frozen_string_literal: true

require 'concerns/network_csv_maker'

#
# One time job to recreate network snapshots for network ids 1 to 27
#
class RecreateMissingNetworks
  include Sidekiq::Worker

  def perform(ids: 1..27)
    maker = NetworkCsvMaker.new
    ids.each do |network_id|
      maker.update_network_snapshot(network_id)
    end
  end
end
