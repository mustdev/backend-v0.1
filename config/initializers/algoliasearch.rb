# frozen_string_literal: true

if ENV['BACKEND_URL'] != 'test'
  AlgoliaSearch.configuration = {
    application_id: ENV['ALGOLIA_APP_ID'],
    api_key: ENV['ALGOLIA_API_TOKEN'],
    connect_timeout: 2,
    receive_timeout: 30,
    send_timeout: 30,
    batch_timeout: 120,
    search_timeout: 5
  }
end
