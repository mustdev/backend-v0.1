# frozen_string_literal: true

if LogStasher.enabled?
  LogStasher.add_custom_fields do |fields|
    # This block is run in application_controller context,
    # so you have access to all controller methods
    fields[:user] = if current_user
                      current_user && current_user.email
                    else
                      '-'
                    end
    #
    # if params.key?(:password)
    #   params[:password] = "********"
    # end
    #
    # if params.key?(:password_confirmation)
    #   params[:password_confirmation] = "********"
    # end

    fields[:params] = params
  end

  LogStasher.add_custom_fields_to_request_context do |fields|
    # This block is run in application_controller context,
    # so you have access to all controller methods
    # You can log custom request fields using this block
    fields[:user] = if current_user
                      current_user && current_user.email
                    else
                      '-'
                    end
    fields[:params] = params
  end
end
