#!/usr/bin/env python3

import os, sys

if os.environ['ELASTICSEARCH_HOST'] and os.environ['ELASTICSEARCH_USER'] and os.environ['ELASTICSEARCH_PASSWORD']:
    print("Elasticsearch host is set. Configuring logstash to send messages...")
    with open('./config/logstash.conf', "r+") as f:
        config = f.read()
        config = config.replace('ELASTICSEARCH_HOST', os.environ['ELASTICSEARCH_HOST'])
        config = config.replace('ELASTICSEARCH_USER', os.environ['ELASTICSEARCH_USER'])
        config = config.replace('ELASTICSEARCH_PASSWORD', os.environ['ELASTICSEARCH_PASSWORD'])
        f.seek(0)
        f.write(config)
        f.truncate()
    sys.exit(0)

sys.exit(1)
