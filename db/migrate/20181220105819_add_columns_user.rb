# frozen_string_literal: true

class AddColumnsUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :first_name, :string
    add_column :users, :last_name, :string
    add_column :users, :nickname, :string
    add_column :users, :age, :decimal
    add_column :users, :social_cat, :string
    add_column :users, :profession, :string
    add_column :users, :country, :string
    add_column :users, :city, :string
    add_column :users, :address, :string
    add_column :users, :phone_number, :string
    add_column :users, :bio, :string
    add_column :users, :allow_password_change, :boolean
    add_column :users, :feed_id, :integer
  end
end
