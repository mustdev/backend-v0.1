# frozen_string_literal: true

class CreateFeeds < ActiveRecord::Migration[5.2]
  def change
    create_table :feeds do |t|
      t.timestamps
      t.belongs_to :user
      t.belongs_to :project
      t.belongs_to :community
      t.string :object_type
    end
  end
end
