# frozen_string_literal: true

class AddMissingcolumnsToProjComm2 < ActiveRecord::Migration[5.2]
  def change
    add_column :projects, :address, :string
    add_column :projects, :city, :string
    add_column :projects, :country, :string
    add_column :communities, :address, :string
    add_column :communities, :city, :string
    add_column :communities, :country, :string
  end
end
