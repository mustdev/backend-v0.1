# frozen_string_literal: true

class AddUserBanerUrl < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :banner_url, :string
  end
end
