# frozen_string_literal: true

class FixAge < ActiveRecord::Migration[5.2]
  def change
    change_column :users, :age, :integer
  end
end
