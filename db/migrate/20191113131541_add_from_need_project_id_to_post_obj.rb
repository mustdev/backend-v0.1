# frozen_string_literal: true

class AddFromNeedProjectIdToPostObj < ActiveRecord::Migration[5.2]
  def change
    add_column :posts, :from_need_project_id, :string
  end
end
