# frozen_string_literal: true

class AddDetailedDescToProj < ActiveRecord::Migration[5.2]
  def change
    add_column :projects, :desc_elevator_pitch, :string
    add_column :projects, :desc_problem_statement, :string
    add_column :projects, :desc_objectives, :string
    add_column :projects, :desc_state_art, :string
    add_column :projects, :desc_progress, :string
    add_column :projects, :desc_stakeholder, :string
    add_column :projects, :desc_impact_strat, :string
    add_column :projects, :desc_ethical_statement, :string
    add_column :projects, :desc_sustainability_scalability, :string
    add_column :projects, :desc_communication_strat, :string
    add_column :projects, :desc_funding, :string
  end
end
