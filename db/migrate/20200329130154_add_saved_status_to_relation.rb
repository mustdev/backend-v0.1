# frozen_string_literal: true

class AddSavedStatusToRelation < ActiveRecord::Migration[5.2]
  def change
    add_column :relations, :saved, :boolean
    add_index :relations, %w[resource_type resource_id]
  end
end
