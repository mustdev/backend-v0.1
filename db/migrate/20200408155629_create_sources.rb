# frozen_string_literal: true

class CreateSources < ActiveRecord::Migration[5.2]
  def change
    create_table :sources do |t|
      t.string :title
      t.string :url

      t.bigint :sourceable_id
      t.string :sourceable_type
      t.timestamps
    end
  end
end
