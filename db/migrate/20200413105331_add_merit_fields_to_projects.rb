# frozen_string_literal: true

class AddMeritFieldsToProjects < ActiveRecord::Migration[5.2]
  def change
    add_column :projects, :sash_id, :integer
    add_column :projects, :level,   :integer, default: 0
  end
end
