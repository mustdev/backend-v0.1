# frozen_string_literal: true

class RepairRessources < ActiveRecord::Migration[5.2]
  Ressource.all.each do |r|
    Ressourceship.create(ressourceable_type: r.ressourceable_type, ressourceable_id: r.ressourceable_id, ressource_id: r.id)
  end
end
