# frozen_string_literal: true

class InitRecsys < ActiveRecord::Migration[5.2]
  # User.all.each do |user|
  #   user.skills.each do |skill|
  #     user.add_edge(skill, "has_skill")
  #   end
  #   user.interests.each do |interest|
  #     user.add_edge(interest, "has_interest")
  #   end
  #   user.ressources.each do |ressource|
  #     user.add_edge(ressource, "has_ressource")
  #   end
  # end
  #
  # Relation.all.each do |relation|
  #   unless relation.resource.nil?
  #     if relation.follows
  #       relation.user.add_edge(relation.resource, "has_followed")
  #     end
  #     if relation.has_clapped
  #       relation.user.add_edge(relation.resource, "has_clapped")
  #     end
  #     if relation.saved
  #       relation.user.add_edge(relation.resource, "has_saved")
  #     end
  #   else
  #     relation.destroy
  #   end
  # end
  #
  # Post.all.each do |post|
  #   from = post.from_object.constantize.find_by(id: post.from_id)
  #   post.user.add_edge(post, "is_author_of")
  #   if from
  #     post.user.add_edge(from, "has_posted_on")
  #   end
  #   post.comments.each do |comment|
  #     comment.user.add_edge(post, "has_commented_on")
  #     comment.user.add_edge(comment, "is_author_of")
  #     comment.add_edge(post, "is_comment_of")
  #   end
  #   post.mentions.each do |mention|
  #     object = mention[:obj_type].humanize.constantize.where(id: mention[:obj_id]).first
  #     unless object.nil?
  #       post.user.add_edge(object, "has_mentionned")
  #     end
  #   end
  # end
  #
  # Need.all.each do |need|
  #   need.user.add_edge(need, "is_author_of")
  #   need.skills.each do |skill|
  #     need.add_edge(skill, "has_skill")
  #   end
  #   need.ressources.each do |ressource|
  #     need.add_edge(ressource, "has_ressource")
  #   end
  # end
  #
  # Project.all.each do |project|
  #   project.creator.add_edge(project, "is_author_of")
  #   project.users.each do |user|
  #     if user.has_role? :pending, project
  #       user.add_edge(project, "is_pending_of")
  #     end
  #     if user.has_role? :member, project
  #       user.add_edge(project, "is_member_of")
  #     end
  #     if user.has_role? :admin, project
  #       user.add_edge(project, "is_admin_of")
  #     end
  #     if user.has_role? :owner, project
  #       user.add_edge(project, "is_owner_of")
  #     end
  #   end
  #   project.needs.each do |need|
  #     project.add_edge(need, "has_need")
  #   end
  #   project.skills.each do |skill|
  #     project.add_edge(skill, "has_skill")
  #   end
  #   project.interests.each do |interest|
  #     project.add_edge(interest, "has_interest")
  #   end
  # end
  #
  # Community.all.each do |community|
  #   community.creator.add_edge(community, "is_author_of")
  #   community.users.each do |user|
  #     if user.has_role? :pending, community
  #       user.add_edge(community, "is_pending_of")
  #     end
  #     if user.has_role? :member, community
  #       user.add_edge(community, "is_member_of")
  #     end
  #     if user.has_role? :admin, community
  #       user.add_edge(community, "is_admin_of")
  #     end
  #     if user.has_role? :owner, community
  #       user.add_edge(community, "is_owner_of")
  #     end
  #   end
  #   community.skills.each do |skill|
  #     community.add_edge(skill, "has_skill")
  #   end
  #   community.interests.each do |interest|
  #     community.add_edge(interest, "has_interest")
  #   end
  #   community.ressources.each do |ressource|
  #     community.add_edge(ressource, "has_ressource")
  #   end
  # end
  #
  # Challenge.all.each do |challenge|
  #   challenge.users.each do |user|
  #     if user.has_role? :pending, challenge
  #       user.add_edge(challenge, "is_pending_of")
  #     end
  #     if user.has_role? :member, challenge
  #       user.add_edge(challenge, "is_member_of")
  #     end
  #     if user.has_role? :admin, challenge
  #       user.add_edge(challenge, "is_admin_of")
  #     end
  #     if user.has_role? :owner, challenge
  #       user.add_edge(challenge, "is_owner_of")
  #     end
  #   end
  #   challenge.skills.each do |skill|
  #     challenge.add_edge(skill, "has_skill")
  #   end
  #   challenge.interests.each do |interest|
  #     challenge.add_edge(interest, "has_interest")
  #   end
  #   challenge.projects.each do |project|
  #     project.add_edge(challenge, "is_part_of")
  #   end
  # end
  #
  # Program.all.each do |program|
  #   program.users.each do |user|
  #     if user.has_role? :pending, program
  #       user.add_edge(program, "is_pending_of")
  #     end
  #     if user.has_role? :member, program
  #       user.add_edge(program, "is_member_of")
  #     end
  #     if user.has_role? :admin, program
  #       user.add_edge(program, "is_admin_of")
  #     end
  #     if user.has_role? :owner, program
  #       user.add_edge(program, "is_owner_of")
  #     end
  #   end
  #   program.challenges.each do |challenge|
  #     challenge.add_edge(program, "is_part_of")
  #   end
  # end
end
