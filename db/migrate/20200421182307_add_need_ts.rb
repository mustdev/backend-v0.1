# frozen_string_literal: true

class AddNeedTs < ActiveRecord::Migration[5.2]
  def change
    add_column :needs, :created_at, :datetime
    add_column :needs, :updated_at, :datetime
  end
end
