class AddMissingProgramFields < ActiveRecord::Migration[5.2]
  def change
    add_column :programs, :contact_email, :string
    add_column :programs, :meeting_information, :string
  end
end
