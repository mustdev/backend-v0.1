# frozen_string_literal: true

class AddShowFeaturedObjectsToSpaces < ActiveRecord::Migration[5.2]
  def change
    add_column :spaces, :show_featured_programs, :boolean, default: false
    add_column :spaces, :show_featured_challenges, :boolean, default: false
    add_column :spaces, :show_featured_projects, :boolean, default: false
    add_column :spaces, :show_featured_needs, :boolean, default: false
  end
end
