# frozen_string_literal: true

class AddIndexesForShortnames < ActiveRecord::Migration[5.2]
  def change
    # Speed up the collection/getid/:short_title endpoint
    # by adding indexes to the field to be search on
    # Let's not full table scan each time :D
    add_index(:challenges, :short_title)
    add_index(:communities, :short_title)
    add_index(:programs, :short_title)
    add_index(:projects, :short_title)
    add_index(:spaces, :short_title)
  end
end
