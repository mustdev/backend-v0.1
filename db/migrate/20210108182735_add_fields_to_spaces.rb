class AddFieldsToSpaces < ActiveRecord::Migration[6.1]
  def change
    add_column :spaces, :code_of_conduct, :text
    add_column :spaces, :onboarding_steps, :text
  end
end
