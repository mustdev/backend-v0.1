class AddIndexOnProgramIdToChallenges < ActiveRecord::Migration[6.1]
  def change
    add_index :challenges, :program_id
  end
end
