class CreateAffiliationsTable < ActiveRecord::Migration[6.1]
  def change
    create_table :affiliations do |t|
      t.references :affiliate, polymorphic: true
      t.references :parent, polymorphic: true

      t.timestamps
    end
  end
end
