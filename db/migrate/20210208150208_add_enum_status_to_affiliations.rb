class AddEnumStatusToAffiliations < ActiveRecord::Migration[6.1]
  def change
    add_column :affiliations, :status, :integer, default: 0 # default to "pending"
  end
end
