# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Api::PostsController, type: :controller do
  before do
    @user = create(:confirmed_user)
    sign_in @user
    @post = build(:post)
    @post.feed_id = @user.feed.id
  end

  describe 'Will trigger badges' do
    it 'at 1 post first-post badge' do
      post :create, params: { post: @post.attributes }
      json_response = JSON.parse(response.body)
      expect(response.status).to eq 201
      expect(json_response['data']).to eq 'Your post has been published'
      @user.reload
      expect(@user.has_badge?('first-post')).to be_truthy
    end

    it 'at 10 post first-post badge' do
      create_list(:post, 9, feed_id: @user.feed.id, user_id: @user.id)
      post :create, params: { post: @post.attributes }
      json_response = JSON.parse(response.body)
      expect(response.status).to eq 201
      expect(json_response['data']).to eq 'Your post has been published'
      @user.reload
      expect(@user.has_badge?('first-post')).to be_truthy
      expect(@user.has_badge?('ten-post')).to be_truthy
    end
  end
end
