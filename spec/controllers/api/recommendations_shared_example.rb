# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'an object with recommendations' do |factory|
  before do
    @object = create(factory)
    @object2 = create(factory)
    @object3 = create(factory)
  end

  context 'Signed in' do
    before do
      @user = create(:confirmed_user)
      sign_in @user
      RecsysResult.create(sourceable_node: @user, targetable_node: @object, value: 0.3)
      RecsysResult.create(sourceable_node: @user, targetable_node: @object2, value: 0.3)
      RecsysResult.create(sourceable_node: @user, targetable_node: @object3, value: 0.3)
      RecsysResult.create(sourceable_node: @object, targetable_node: @object2, value: 0.3)
      RecsysResult.create(sourceable_node: @object, targetable_node: @object3, value: 0.3)
    end

    it 'returns the recommendations for the object type' do
      get :recommended
      expect(response).to have_http_status :ok
      json_response = JSON.parse(response.body)
      expect(json_response.count).to eq 3
    end

    it 'returns the recommendations for the object type' do
      get :similar, params: { id: @object.id }
      expect(response).to have_http_status :ok
      json_response = JSON.parse(response.body)
      expect(json_response.count).to eq 2
    end
  end

  # Testing the Auth methods

  context 'Not Signed In' do
    before do
      @user = create(:confirmed_user)
      RecsysResult.create(sourceable_node: @user, targetable_node: @object, value: 0.3)
      RecsysResult.create(sourceable_node: @user, targetable_node: @object2, value: 0.3)
      RecsysResult.create(sourceable_node: @user, targetable_node: @object3, value: 0.3)
      RecsysResult.create(sourceable_node: @object, targetable_node: @object2, value: 0.3)
      RecsysResult.create(sourceable_node: @object, targetable_node: @object3, value: 0.3)
    end

    it 'should return unauthorized' do
      get :recommended
      expect(response).to have_http_status :unauthorized
    end

    it 'returns the recommendations for the object type' do
      get :similar, params: { id: @object.id }
      expect(response).to have_http_status :ok
      json_response = JSON.parse(response.body)
      expect(json_response.count).to eq 2
    end
  end
end
