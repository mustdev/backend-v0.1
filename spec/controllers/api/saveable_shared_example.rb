# frozen_string_literal: true

require 'rails_helper'

RSpec.shared_examples 'an object with saveable' do |factory|
  before do
    @object = create(factory)
  end

  context 'Signed in' do
    before do
      @user = create(:confirmed_user)
      sign_in @user
    end

    describe '#save' do
      it "should save the #{described_class}" do
        put :save, params: { id: @object.id }
        expect(response).to have_http_status :ok
        expect(@user.saved?(@object)).to be true
        expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user.id, targetable_node_type: @object.class.name, targetable_node_id: @object.id, relation_type: 'has_saved')).to exist
      end
    end

    describe '#unsave' do
      before do
        @relation = @user.owned_relations.create(resource: @object, saved: true)
        @user.add_edge(@object, 'has_saved')
      end

      it "should unsave the #{described_class}" do
        expect(@user.saved?(@object)).to be true
        delete :save, params: { id: @object.id }
        expect(response).to have_http_status :ok
        expect(@user.saved?(@object)).to be false
        expect(RecsysDatum.where(sourceable_node_type: 'User', sourceable_node_id: @user.id, targetable_node_type: @object.class.name, targetable_node_id: @object.id, relation_type: 'has_saved')).not_to exist
      end
    end

    describe '#is_save' do
      before do
        @relation = @user.owned_relations.create(resource: @object, saved: true)
      end

      it 'should answer true' do
        get :save, params: { id: @object.id }
        json_response = JSON.parse(response.body)
        expect(response).to have_http_status :ok
        expect(json_response['has_saved']).to be true
      end

      it 'should answer false' do
        @relation.saved = false
        @relation.save
        get :save, params: { id: @object.id }
        json_response = JSON.parse(response.body)
        expect(response).to have_http_status :ok
        expect(json_response['has_saved']).to eq false
      end

      it 'should answer false' do
        @relation.delete
        get :save, params: { id: @object.id }
        json_response = JSON.parse(response.body)
        expect(response).to have_http_status :ok
        expect(json_response['has_saved']).to eq false
      end
    end
  end

  # Testing the Auth methods

  context 'Not Signed In' do
    it 'should require auth' do
      put :save, params: { id: @object.id }
      expect(response).to have_http_status :unauthorized
    end

    it 'should require auth' do
      delete :save, params: { id: @object.id }
      expect(response).to have_http_status :unauthorized
    end

    it 'should require auth' do
      get :save, params: { id: @object.id }
      expect(response).to have_http_status :unauthorized
    end
  end
end
