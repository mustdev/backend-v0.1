# frozen_string_literal: true

FactoryBot.define do
  factory :board do
    title { FFaker::Movie.title }
    description { FFaker::DizzleIpsum.paragraph }

    after :create do |board|
      board.users << create(:confirmed_user)
      board.users << create(:confirmed_user)
      board.users << create(:confirmed_user)
      board.boardable = create(:program)
      board.save
    end
  end
end
