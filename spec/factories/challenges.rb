# frozen_string_literal: true

FactoryBot.define do
  factory :challenge do
    title { FFaker::Movie.unique.title }
    short_title { FFaker::Internet.unique.user_name }
    description { "Challenge's Description" }
    short_description { "challenge's short description" }

    after :create do |challenge|
      skills = create_list(:skill, 5)
      skills.map do |skill|
        challenge.skills << skill # has_many
      end
      user = create(:confirmed_user)
      user.add_role :member, challenge
      user.add_role :admin, challenge
      user.add_role :owner, challenge
    end
  end
end
