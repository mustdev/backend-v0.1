# frozen_string_literal: true

FactoryBot.define do
  factory :customdata do
    # Need to set 2 parameters !
    # customfield_id: ID of an existing custofield
    # user_id: id of an existing user

    value { FFaker::DizzleIpsum.paragraph }
  end
end
