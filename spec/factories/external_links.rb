# frozen_string_literal: true

FactoryBot.define do
  factory :external_link do
    url { FFaker::Internet.http_url }
    name { 'custom' }

    after :create do |link|
      link.icon.attach(
        io: File.open(Rails.root.join('spec', 'test-image.png')),
        filename: 'test-image.png'
      )
    end
  end
end
