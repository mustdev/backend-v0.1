# frozen_string_literal: true

FactoryBot.define do
  factory :feed do
    association :feedable, factory: :user # override if necessary
  end
end
