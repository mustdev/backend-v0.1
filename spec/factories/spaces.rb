# frozen_string_literal: true

FactoryBot.define do
  factory :space do
    description { 'MyString' }
    title { FFaker::Movie.unique.title }
    short_title { FFaker::Internet.unique.user_name }
    short_description { FFaker::Movie.title }

    after :create do |space|
      user = create(:confirmed_user)
      space.users << user
      user.add_role :member, space
      user.add_role :admin, space
      user.add_role :owner, space
    end
  end
end
