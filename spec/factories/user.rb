# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    email { FFaker::Internet.unique.free_email }
    first_name { FFaker::Name.first_name }
    last_name { FFaker::Name.last_name }
    nickname { FFaker::Internet.unique.user_name }
    password { 'password' }
    password_confirmation { 'password' }
    age { rand(56) }
    profession { FFaker::Job.title }
    social_cat { FFaker::Education.degree }
    country { User::VALID_COUNTRY_NAMES.sample }
    city { FFaker::Address.city }
    address { FFaker::Address.street_address }
    bio { FFaker::DizzleIpsum.sentence }

    after :create do |user|
      skills = create_list(:skill, 5)
      skills.map do |skill|
        user.skills << skill # has_many
      end
      ressources = create_list(:ressource, 5)
      ressources.map do |ressource|
        user.ressources << ressource # has_many
      end
    end
  end

  factory :confirmed_user, aliases: %i[creator author], parent: :user do
    after(:create, &:confirm)
  end

  factory :deleted_user, parent: :user do
    id { -1 }
    email { 'deleted@user.com' }
    first_name { 'Deleted' }
    last_name { 'User' }
    nickname { 'deleted_user' }
    password { 'password' }
    password_confirmation { 'password' }
  end

  #
  # after :create do |user|
  #   skills = create_list(:skill, 5)
  #   skills.map do |skill|
  #     user.skills << skill   # has_many
  #   end
  # end
  #
  # after :create do |user|
  #   ressources = create_list(:ressource, 5, ressourceable_id: user.id, ressourceable_type: "Need")
  #   ressources.map do |ressource|
  #     user.ressources << ressource   # has_many
  #   end
  # end
end
