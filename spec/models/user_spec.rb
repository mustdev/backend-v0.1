# frozen_string_literal: true

require 'rails_helper'
require_relative 'shared_examples/feedable'

RSpec.describe User, type: :model do
  it_behaves_like 'a model that can have a feed', :user

  before(:each) do
    @shannon = create(:user, first_name: 'Shannon')
  end

  it 'needs confirmation' do
    expect(@shannon.confirmed?).to be false
  end

  it 'is confirmed' do
    @shannon.confirm
    expect(@shannon.confirmed?).to be true
  end

  it 'is valid with valid attributes' do
    expect(@shannon).to be_valid
  end

  it 'has a unique email' do
    user2 = build(:user, email: @shannon.email)
    expect(user2).not_to be_valid
  end

  # TODO: must only work on update not create
  # it "has a unique nickname" do
  #   user2 = build(:user, nickname: @shannon.nickname)
  #   expect(user2).not_to be_valid
  # end

  it 'is not valid without a password' do
    user2 = build(:user, password: nil)
    expect(user2).not_to be_valid
  end

  it 'is not valid without an email' do
    user2 = build(:user, email: nil)
    expect(user2).not_to be_valid
  end

  it 'sets the user status to active upon create' do
    new_user = create(:user)
    expect(new_user).to be_active
  end

  it 'default settings' do
    @shannon.settings
    expect(@shannon.settings.enabled).to be_truthy
    expect(@shannon.settings.categories.notification.enabled).to be_truthy
    expect(@shannon.settings.categories.notification.delivery_methods.email.enabled).to be_truthy
    expect(@shannon.settings.delivery_methods.email.enabled).to be_truthy
  end

  describe 'model relations' do
    it 'finds the projects where user has a role' do
      project = create(:project)
      user = create(:user)
      user.add_role(:admin, project)
      user.add_role(:member, project)
      expect(user.projects).to include(project)
      expect(user.projects.count).to eq(1)
    end

    it 'finds the challenges where a user has a role' do
      challenge = create(:challenge)
      user = create(:user)
      user.add_role(:admin, challenge)
      user.add_role(:member, challenge)
      expect(user.challenges).to include(challenge)
      expect(user.challenges.count).to eq(1)
    end

    it 'finds the communities where a user has a role' do
      community = create(:community)
      user = create(:user)
      user.add_role(:admin, community)
      user.add_role(:member, community)
      expect(user.communities).to include(community)
      expect(user.communities.count).to eq(1)
    end

    it 'finds the programs where a user has a role' do
      program = create(:program)
      user = create(:user)
      user.add_role(:admin, program)
      user.add_role(:member, program)
      expect(user.programs).to include(program)
      expect(user.programs.count).to eq(1)
    end

    it 'finds the needs where a user has a role' do
      need = create(:need)
      user = create(:user)
      user.add_role(:admin, need)
      user.add_role(:member, need)
      expect(user.needs).to include(need)
      expect(user.needs.count).to eq(1)
    end
  end

  describe '#deleted?' do
    it "is deleted if it's archived" do
      @shannon.archived!
      expect(@shannon).to be_deleted
    end
  end

  describe '#make_address' do
    it 'combines address, city, country into an address string' do
      user = build(:user,
                   address: '123 Green Street',
                   city: 'Chicago',
                   country: 'U.S.A.')
      expect(user.make_address).to eq('123 Green Street, Chicago, U.S.A.')
    end

    it 'uses the IP if there is no city' do
      user = build(:user, city: nil, ip: '1234')
      expect(user.make_address).to eq('1234')
    end
  end

  describe 'coutry validation' do
    it 'validates country if present' do
      user = build(:user, country: 'USA')
      expect(user).to_not be_valid
      expect(user.errors.full_messages).to include('Country is not included in the list')
    end

    it 'is valid if country is in list' do
      user = build(:user, country: 'United States')
      expect(user).to be_valid
    end

    it 'allows country to be nil' do
      user = build(:user, country: nil)
      expect(user).to be_valid
    end
  end

  describe '#clapped?(object)' do
    it 'is true if the user has clapped the object' do
      post = create(:post)
      @shannon.owned_relations.create(resource: post, has_clapped: true)

      expect(@shannon.clapped?(post)).to eq(true)
    end

    it 'is false if the user has not clapped the object' do
      post = create(:post)

      expect(@shannon.clapped?(post)).to eq(false)
    end
  end

  describe '#saved?(object)' do
    it 'is true if the user has saved the object' do
      post = create(:post)
      @shannon.owned_relations.create(resource: post, saved: true)

      expect(@shannon.saved?(post)).to eq(true)
    end

    it 'is false if the user has not saved the object' do
      post = create(:post)

      expect(@shannon.saved?(post)).to eq(false)
    end
  end

  describe '#follows?(object)' do
    it 'is true if the user has followed the object' do
      post = create(:post)
      @shannon.owned_relations.create(resource: post, follows: true)

      expect(@shannon.follows?(post)).to eq(true)
    end

    it 'is false if the user has not followed the object' do
      post = create(:post)

      expect(@shannon.follows?(post)).to eq(false)
    end
  end

  describe 'logo_url_sm' do
    it 'picks a pseudo-random avatar image' do
      expect(@shannon.logo_url_sm).to match(%r{http://localhost:3001/assets/default-user-\d+-\w*.png})
    end

    it 'is always the same' do
      # test to ensure random gets re-seeded each time it's called
      expect(@shannon.logo_url_sm).to eq(@shannon.logo_url_sm)
    end

    it 'calls default_logo_url if avatar.attachment is not available' do
      expect(@shannon).to receive(:default_logo_url).with(no_args)
      @shannon.logo_url_sm
    end

    it "uses the avatar image if it's present" do
      @shannon.avatar.attach(
        io: File.open(Rails.root.join('spec', 'test-image.png')),
        filename: 'test-image'
      )

      expect(@shannon).to receive(:avatar_variant_url).with(resize: '40x40^')
      @shannon.logo_url_sm
    end

    xit "uses the avatar image if it's present" do
      @shannon.avatar.attach(
        io: File.open(Rails.root.join('spec', 'test-image.svg')),
        filename: 'test-image'
      )

      expect(@shannon).to receive(:avatar_blob_url)
      @shannon.logo_url_sm
    end
  end

  describe '#has_badge?' do
    it 'is true if the user has the badge' do
      badge = Merit::Badge.create!(
        id: 11,
        name: 'banana',
        description: "It's Yellow!"
      )
      @shannon.add_badge(badge.id)

      expect(@shannon.has_badge?('banana')).to eq(true)
    end

    it 'is false if the user does not have the badge' do
      expect(@shannon.has_badge?('banana')).to eq(false)
    end
  end

  describe '#follow_mutual' do
    before do
      @user0 = create(:confirmed_user)
      @shannon = create(:confirmed_user)
      @user2 = create(:confirmed_user)
      @user3 = create(:confirmed_user)
      @other = create(:confirmed_user)
      @user0.owned_relations.create(resource: @shannon, follows: true)
      @user0.owned_relations.create(resource: @user2, follows: true)
      @user0.owned_relations.create(resource: @user3, follows: true)
      @other.owned_relations.create(resource: @shannon, follows: true)
      @other.owned_relations.create(resource: @user3, follows: true)
    end

    it 'returns the mutual followers between two users' do
      mutual = @user0.follow_mutual(@other)
      expect(mutual.count).to eq(2)
      expect(mutual.pluck(:id)).to include(@shannon.id)
      expect(mutual.pluck(:id)).to include(@user3.id)
      expect(mutual.pluck(:id)).not_to include(@user2.id)
    end

    it 'returns the right count of mutual connection' do
      expect(@user0.follow_mutual_count(@other)).to eq(2)
    end
  end

  describe '#projects_count' do
    it 'returns zero if the user is not associated with a project' do
      # Check that the project count is 0
      expect(@shannon.projects_count).to eq(0)
    end

    it 'does not include projects where  the user\'s role is pending' do
      # Create a project
      project = create(:project, creator: @shannon)
      @shannon.add_role(:member, project)

      # Add a pending user
      user_pending = create(:user)
      user_pending.add_role(:pending, project)

      # We expect the pending user's project count be 0
      expect(user_pending.projects_count).to eq(0)
    end

    it 'returns the count of projects where the user is a member' do
      # Create a project
      project = create(:project, creator: @shannon)
      @shannon.add_role(:member, project)

      # We expect the pending user's project count be 1
      expect(@shannon.projects_count).to eq(1)
    end
  end

  describe '#needs_count' do
    it 'includes needs where the user is a member' do
      create(:need, user: @shannon)
      create(:need)
      expect(@shannon.needs_count).to eq(1)
    end
  end

  describe '#communities_count' do
    it 'includes communities where the user is a member' do
      create(:community, creator: @shannon)
      create(:community)
      expect(@shannon.communities_count).to eq(1)
    end
  end

  describe '#age' do
    it 'defaults to using `age` db field if birth date is not present' do
      josh = create(:user, age: 24, first_name: 'Josh')
      expect(josh.age).to eq(24)
    end

    it 'uses birth_date if present' do
      josh = create(:user, age: nil, first_name: 'Josh', birth_date: Date.today)
      Timecop.freeze(Date.today + 23.years + 1.day) do
        expect(josh.age).to eq(23)
      end
    end
  end

  describe '#is_reviewer' do

    before do
      @shannon = create(:confirmed_user)
      @user = create(:confirmed_user)
    end
    
    it 'find the project where a user is a reviewer' do
      @project = create(:project, creator: @shannon)
      @user.add_role(:reviewer, @project)
      
      expect(@user.has_role?(:reviewer, @project)).to be_truthy
      expect(@user.projects).to include(@project)
      expect(@user.projects.count).to eq(1)
    end
    
    it 'find the program where a user is a reviewer' do
      @program = create(:program)
      @user.add_role(:reviewer, @program)

      expect(@user.has_role?(:reviewer, @program)).to be_truthy
      expect(@user.programs).to include(@program)
      expect(@user.programs.count).to eq(1)
    end

    it 'find the challenge where a user is a reviewer' do
      @challenge = create(:challenge)
      @user.add_role(:reviewer, @challenge)

      expect(@user.has_role?(:reviewer, @challenge)).to be_truthy
      expect(@user.challenges).to include(@challenge)
      expect(@user.challenges.count).to eq(1)
    end

    it 'find the communities where a user is a reviewer' do
      @community = create(:community)
      @user.add_role(:reviewer, @community)

      expect(@user.has_role?(:reviewer, @community)).to be_truthy
      expect(@user.communities).to include(@community)
      expect(@user.communities.count).to eq(1)
    end
  end

  describe '#reviewed?(object)' do
    it 'is true if the user has reviewed the object' do
      project = create(:project)
      @shannon.owned_relations.create(resource: project, reviewed: true)

      expect(@shannon.reviewed?(project)).to eq(true)
    end

    it 'is false if the user has not saved the object' do
      project = create(:project)

      expect(@shannon.reviewed?(project)).to eq(false)
    end
  end

end
