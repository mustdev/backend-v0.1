require 'rails_helper'
require_relative 'api_login'

RSpec.describe 'Affiliations', type: :request do
  let(:bob) { create(:confirmed_user, first_name: 'Bob', email: 'bob@example.com') }

  before do
    api_login(bob)
    @auth_params = get_auth_params_from_login_response_headers(response)
  end

  describe 'create affiliations' do
    it 'requires affiliation in the params' do
      post '/api/affiliations', headers: @auth_params

      expect(JSON.parse(response.body)['errors']).to include('Affiliation must be present')
    end

    it '#create' do
      space = create(:space)
      program = create(:program)

      post '/api/affiliations', headers: @auth_params, params: { affiliation: { parent_id: space.id, parent_type: "Space", affiliate_id: program.id, affiliate_type: "Program" } }

      json_response = JSON.parse(response.body)
      expect(json_response['affiliate_id']).to eq(program.id)
      expect(json_response['affiliate_type']).to eq("Program")
      expect(json_response['parent_id']).to eq(space.id)
      expect(json_response['parent_type']).to eq("Space")
    end

    it 'returns errors if trying to create invalid affiliation' do
      space = create(:space)

      post '/api/affiliations',
        headers: @auth_params,
        params: {
          affiliation: {
            parent_id: space.id,
            parent_type: "Space",
            affiliate_id: space.id,
            affiliate_type: "Space"
          }
        }

        json_response = JSON.parse(response.body)
        expect(json_response["errors"][0]).to match("type invalid, must be one of")
    end

    it 'defaults to pending status' do
      space = create(:space)
      program = create(:program)

      post '/api/affiliations', headers: @auth_params, params: { affiliation: { parent_id: space.id, parent_type: "Space", affiliate_id: program.id, affiliate_type: "Program" } }

      json_response = JSON.parse(response.body)
      expect(json_response['status']).to eq('pending')
    end
  end

  describe '#update' do
    it "can update status" do
      space = create(:space)
      program = create(:program)
      affiliation = space.add_affiliate(program)

      expect {
        put api_affiliation_path(id: affiliation.id), headers: @auth_params, params: {
          affiliation: { status: 'accepted' }
        }
        affiliation.reload
      }.to change { affiliation.status }.from('pending').to('accepted')
    end
  end

  describe 'destroy affiliations' do
    it "destroys a single affiliation" do
      space = create(:space)
      program = create(:program)
      affiliation = space.add_affiliate(program)

      expect {
        delete api_affiliation_path(id: affiliation.id), headers: @auth_params
      }.to change { Affiliation.count }.from(1).to(0)
    end
  end
end
