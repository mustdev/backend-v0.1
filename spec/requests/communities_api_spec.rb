# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'CommunityApiUnprotected', type: :request do
  before do
    @creator = create(:user)
    [@creator, @creator, @creator].map do |_creator|
      create(:community, creator_id: @creator.id)
    end
  end

  it 'gets the index' do
    path '/api/communities' do
      get(summary: 'Get community index') do
        response(200, description: 'Get community indexes') do
          body = JSON(response.body)
          expect(body.count).to eq(3)
        end
      end

      get(summary: 'test if archived not rendered') do
        before do
          create(:archived_community)
        end

        response(200, description: 'Get community indexes') do
          body = JSON(response.body)
          expect(body.count).to eq(3)
        end
      end
    end
  end

  it 'gets one community' do
    path '/api/communities/{id}' do
      parameter :id, in: :path, type: :integer, required: true, description: 'Community ID'

      response(200, description: 'Return the selected community') do
        let(:id) { communities[0].id }
        it 'is the correct community' do
          body = JSON(response.body)
          expect(body['id']).to eq(communities[0].id)
        end
      end

      response(404, description: 'Community not found') do
        let(:id) { 999 }
      end
    end
  end
end

RSpec.describe 'CommunityApiProtected', type: :request do
  it 'creates a community' do
    @user = create(:confirmed_user)

    path '/api/communities' do
      post(summary: 'Unauthorized') do
        parameter :data,
                  in: :body,
                  required: true

        response(401, description: 'Unauthorized') do
          let(:data) do
            {
              community: {
                status: 'draft',
                title: 'A long Title',
                short_title: 'AshortTitle',
                logo_url: 'https://robohash.org/voluptatumundesed.png?size=300x300',
                description: 'A description',
                short_description: 'A short description',
                is_private: false,
                interests: [1, 2],
                skills: ['Python', '3D printing']
              }
            }
          end
        end
      end

      sign_in @user

      path '/api/communities' do
        post(summary: 'Unauthorized') do
          parameter :data,
                    in: :body,
                    required: true

          response(201, description: 'Unauthorized') do
            let(:data) do
              {
                community: {
                  status: 'draft',
                  title: 'A long Title',
                  short_title: 'AshortTitle',
                  logo_url: 'https://robohash.org/voluptatumundesed.png?size=300x300',
                  description: 'A description',
                  short_description: 'A short description',
                  is_private: false,
                  interests: [1, 2],
                  skills: ['Python', '3D printing']
                }
              }
            end
            it 'has the right creator_id' do
              body = JSON(response.body)
              expect(body['creator_id']).to eq(@user.id)
            end
            it 'has the right data' do
              body = JSON(response.body)
              expect(body['title']).to eq('A long Title')
              expect(body['short_title']).to eq('AshortTitle')
              expect(body['logo_url']).to eq('https://robohash.org/voluptatumundesed.png?size=300x300')
              expect(body['description']).to eq('A description')
              expect(body['short_description']).to eq('A short description')
              expect(body['is_private']).to eq(false)
              expect(body['interests']).to eq([1, 2])
              expect(body['skills']).to eq(['Python', '3D printing'])
            end
          end
        end
      end
    end
  end

  it 'can destroy a community' do
    @creator = create(:confirmed_user)
    @community = create(:community, creator_id: @creator.id)

    path '/api/community/{id}' do
      delete
    end
  end

  it 'is unauthorized to update a community' do
    @user = create(:user)
    @community = create(:community, creator_id: @user.id)

    path '/api/communities/{id}' do
      patch(summary: 'update a community') do
        parameter :id, in: :path, type: :integer, required: true, description: 'Community ID'
        parameter :data,
                  in: :body,
                  required: true
        response(401, description: 'Unauthorized') do
          let(:data) do
            {
              community: {
                status: 'draft',
                title: 'A long Title',
                short_title: 'AshortTitle',
                logo_url: 'https://robohash.org/voluptatumundesed.png?size=300x300',
                description: 'A description',
                short_description: 'A short description',
                is_private: false,
                interests: [1, 2],
                skills: ['Python', '3D printing']
              }
            }
          end
        end
      end
    end
  end

  it 'is forbidden to update a community' do
    @user = create(:user)
    @user2 = create(:user)
    @community = create(:community, creator_id: @user2.id)

    sign_in @user

    path '/api/communities/{id}' do
      patch(summary: 'Update a community') do
        parameter :id, in: :path, type: :integer, required: true, description: 'Community ID'
        parameter :data,
                  in: :body,
                  required: true
        response(403, description: 'Unauthorized') do
          let(:data) do
            {
              community: {
                status: 'draft',
                title: 'A long Title',
                short_title: 'AshortTitle',
                logo_url: 'https://robohash.org/voluptatumundesed.png?size=300x300',
                description: 'A description',
                short_description: 'A short description',
                is_private: false,
                interests: [1, 2],
                skills: ['Python', '3D printing']
              }
            }
          end
        end
      end
    end
  end

  it 'creator can update community' do
    @user = create(:confirmed_user)
    @community = create(:community, creator_id: @user.id)

    sign_in @user

    path '/api/communities/{id}' do
      patch(summary: 'Update a community') do
        parameter :id, in: :path, type: :integer, required: true, description: 'Community ID'
        parameter :data,
                  in: :body,
                  required: true
        response(200, description: 'Success') do
          let(:data) do
            {
              community: {
                status: 'draft',
                title: 'A long Title',
                short_title: 'AshortTitle',
                logo_url: 'https://robohash.org/voluptatumundesed.png?size=300x300',
                description: 'A description',
                short_description: 'A short description',
                is_private: false,
                interests: [1, 2],
                skills: ['Python', '3D printing']
              }
            }
          end
          it 'is updated' do
            body = JSON(response.body)
            expect(body['title']).to eq('A long Title')
            expect(body['short_title']).to eq('AshortTitle')
            expect(body['logo_url']).to eq('https://robohash.org/voluptatumundesed.png?size=300x300')
            expect(body['description']).to eq('A description')
            expect(body['short_description']).to eq('A short description')
            expect(body['is_private']).to eq(false)
            expect(body['interests']).to eq([1, 2])
            expect(body['skills']).to eq(['Python', '3D printing'])
          end
        end
      end
    end
  end

  it 'admin can update community' do
    @user = create(:confirmed_user)
    @admin = create(:confirmed_user)
    @community = create(:community, creator: @user)
    @admin.add_role(:admin, @community)

    sign_in @admin

    path '/api/communities/{id}' do
      patch(summary: 'Update the community') do
        parameter :id, in: :path, type: :integer, required: true, description: 'Community ID'
        parameter :data,
                  in: :body,
                  required: true
        response(200, description: 'Success') do
          let(:data) do
            {
              community: {
                status: 'draft',
                title: 'A long Title',
                short_title: 'AshortTitle',
                logo_url: 'https://robohash.org/voluptatumundesed.png?size=300x300',
                description: 'A description',
                short_description: 'A short description',
                is_private: false,
                interests: [1, 2],
                skills: ['Python', '3D printing']
              }
            }
          end
          it 'is updated' do
            body = JSON(response.body)
            expect(body['title']).to eq('A long Title')
            expect(body['short_title']).to eq('AshortTitle')
            expect(body['logo_url']).to eq('https://robohash.org/voluptatumundesed.png?size=300x300')
            expect(body['description']).to eq('A description')
            expect(body['short_description']).to eq('A short description')
            expect(body['is_private']).to eq(false)
            expect(body['interests']).to eq([1, 2])
            expect(body['skills']).to eq(['Python', '3D printing'])
          end
        end
      end
    end
  end

  it 'can join a community' do
    @creator = create(:confirmed_user)
    @member = create(:confirmed_user)
    @community = create(:community, creator_id: @creator.id)

    sign_in @member

    path '/api/community/{id}/join' do
      put(summary: 'Join community') do
        parameter :id, in: :path, type: :integer, required: true, description: 'Community ID'

        response(200, description: 'Success')

        it 'has joined' do
          @community.reload
          expect(@community.users.include?(@member)).to be_truthy
          expect(UsersCommunity.where(community_id: @community.id, user_id: @member.id).first.role).to eq('member')
        end
      end
    end
  end

  it 'can leave a community' do
    @creator = create(:confirmed_user)
    @member = create(:confirmed_user)
    @community = create(:community, creator_id: @creator.id)

    sign_in @member

    path '/api/community/{id}/leave' do
      put(summary: 'Leave the community') do
        parameter :id, in: :path, type: :integer, required: true, description: 'Community ID'

        response(200, description: 'Success')

        it 'has leaved' do
          @community.reload
          expect(@community.users.include?(@member)).to be_falsey
        end
      end
    end
  end

  it "can't join a private community" do
    @creator = create(:confirmed_user)
    @member = create(:confirmed_user)
    @community = create(:private_community, creator_id: @creator.id)

    sign_in @member

    path '/api/community/{id}/join' do
      put(summary: 'Join') do
        parameter :id, in: :path, type: :integer, required: true, description: 'Community ID'

        response(200, description: 'Success')

        it 'is pending' do
          @community.reload
          expect(@community.users.include?(@member)).to be_truthy
          expect(UsersCommunity.where(community_id: @community.id, user_id: @member.id).first.role).to eq('pending')
        end
      end
    end
  end

  it 'can invite a user' do
    @creator = create(:confirmed_user)
    @member = create(:confirmed_user)
    @community = create(:private_community, creator_id: @creator.id)

    sign_in @creator

    path '/api/community/{id}/invite' do
      post(summary: 'Invite') do
        parameter :id, in: :path, type: :integer, required: true, description: 'Community ID'
        parameter :data,
                  in: :body,
                  required: true

        response(200, description: 'Success') do
          let(:data) do
            {
              user_id: @member.id
            }
          end
          it 'is a member' do
            @community.reload
            expect(@community.users.include?(@member)).to be_truthy
            expect(UsersCommunity.where(community_id: @community.id, user_id: @member.id).first.role).to eq('member')
          end
        end

        response(200, description: 'Success') do
          let(:data) do
            {
              stranger_email: 'test@test.com'
            }
          end
        end
      end
    end

    sign_out @creator

    # @community.users.delete(@member)

    sign_in @member

    path '/api/community/{id}/invite' do
      post(summary: 'Invite') do
        parameter :id, in: :path, type: :integer, required: true, description: 'Community ID'
        parameter :data,
                  in: :body,
                  required: true

        response(403, description: 'forbidden') do
          let(:data) do
            {
              user_id: @member.id
            }
          end
        end

        it 'is not a member' do
          @community.reload
          expect(@community.users.include?(@member)).to be_falsey
        end
      end
    end
  end
end
